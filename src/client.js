import * as sapper from "@sapper/app"

// Remove initial empty preload to force load
// From https://github.com/timhall/sapper-spa
// Cf https://github.com/sveltejs/sapper/issues/383
if (typeof window !== "undefined" && window.__SAPPER__) {
	window.__SAPPER__.preloaded = []
}

sapper.start({
	target: document.querySelector("#sapper")
})